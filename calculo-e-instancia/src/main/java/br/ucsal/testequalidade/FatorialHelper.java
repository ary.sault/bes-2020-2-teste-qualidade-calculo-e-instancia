package br.ucsal.testequalidade;

/**
 * O uso de métodos de instância/publicos nesta classe é intencional e objetiva
 * ilustrar o uso de mocks para métodos de instância.
 * 
 * @author claudioneiva
 *
 */

public class FatorialHelper {

	// Este método possui um erro de implementação para falhar durante os testes
	// integrados e não falhar durante os testes unitários.
	public Long calcularFatorial(int x) {
		Long fatorial = 1L;
		for (int i = 1; i < x; i++) {
			fatorial *= i;
		}
		return fatorial;
	}

}
